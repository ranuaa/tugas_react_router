import React, { useState } from "react";
import Appbar from "../components/Appbar";
import TextField from '@mui/material/TextField';
import FormModal from "../components/FormModal";
import UserList from "./UserList";
import createPersistedState from 'use-persisted-state';
const useUserState = createPersistedState('usr');

const Homepage = () => {
  const [users, setUsers] = useUserState([])
  const [isUpdate, setIsUpdate] = useState({ id: null, status: false })
  const [formData, setFormData] = useState({
    id: null,
    name: '',
    address: '',
    hobby: ''
  })
  const handleChange = (e) => {
    let data = {...formData}
    data[e.target.name] = e.target.value
    data.id = users.length + 1
    setFormData(data)
  }
  const handleSubmit = (e) => {
    e.preventDefault();
    let data = [...users]
    if(formData.name === ""){
      alert('Masukan Nama User')
      return false
    } 
    if(formData.address === ""){
      alert('Masukan Almat user')
      return false
    } 
    if(formData.hobby === ""){
      alert('Masukan Hobby user')
      return false
    }
    if(isUpdate.status ){
      data.forEach((dat) => {
        if(dat.id === isUpdate.id){
          dat.name = formData.name;
          dat.address = formData.address;
          dat.hobby = formData.hobby;
        }
      })
    }else{
      data.push(formData)
    }
    setUsers(data)
    setFormData({
      name: '',
      address: '',
      hobby: ''
    })
    setIsUpdate({id: null, status : false})
    handleClose()
  }
  const [open, setOpen] = useState(false)
  const handleOpen = () => setOpen(true);
  const handleClose = () => {
    setOpen(false)
  };
  const handleUpdate = (id) => {
    let data = [...users]
    let foundData= data.find(user => user.id === id)
    setFormData({name: foundData.name, address: foundData.address, hobby: foundData.hobby})
    setIsUpdate({id: id, status : true})
    handleOpen()
  }

  const [searchTerm, setSearchTerm] = useState('');
  const [searchResults, setSearchResults] = useState([]);

  const handleSearch = (e) => {
    setSearchTerm(e.target.value);
    const results = users.filter(user =>
      user.name.toLowerCase().includes(searchTerm.toLowerCase())
    );
    if(searchTerm !== ''){
      setSearchResults(results);
    }else{
      setSearchResults('');
    }
  };

  console.log(searchResults)
  return (
  <div>
    <Appbar handleOpen={handleOpen} users={users} handleSearch={handleSearch}/>
    <TextField onChange={handleSearch} id="outlined-basic" placeholder="Search" variant="outlined" sx={{ margin: '10px', textAlign: 'center' }}/>
    <FormModal handleOpen={handleOpen} handleClose={handleClose} open={open} setOpen={setOpen} handleChange={handleChange} handleSubmit={handleSubmit} formData={formData} isUpdate={isUpdate}/>
    <UserList users={users} handleOpen={handleOpen} handleUpdate={handleUpdate} searchResults={searchResults} searchTerm={searchTerm} />
  </div>);
};

export default Homepage;
