import React from "react";
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import StarIcon from '@mui/icons-material/Star';
import { Button } from "@mui/material";
import { padding } from "@mui/system";
import { NavLink } from "react-router-dom";
//comment
const UserList = ({users, handleOpen, handleUpdate,searchResults, searchTerm}) => {
  const searched = searchResults.length !== 0

 if(searched && searchTerm !== ''){
  return (
    <List
    sx={{ width: '50%', bgcolor: 'background.paper', display: 'flex', justifyContent:"space-between", alignItems:'center', flexDirection: 'column', margin: '0 auto' }}
    aria-label="contacts"
  >
    {searchResults.map((user)=> {
        return(
    <ListItem  key={user.id}  sx={{border:'1px solid black'}}>
      <ListItemText primary={user.name} secondary={user.address}/>
      <ListItem sx={{ display: 'flex', justifyContent:"flex-end"}}>
        <div>
        <ListItemText primary={user.hobby} sx={{textAlign: 'center'}} />
      <div>
      <ListItemButton sx={{gap: '10px'}}>
        <NavLink to={{
          pathname: `/view/${user.id}`,
          state: {
            user: user
          }
        }} >
        <Button variant='contained'> View </Button>
        </NavLink>
        <Button onClick={() => {handleOpen(); handleUpdate(user.id)}} variant='contained'>Edit</Button>
      </ListItemButton>
      
      </div>
        </div>
      </ListItem>
    </ListItem>
        )
    })}
  </List>
  );
 }else{
  return (
    <List
    sx={{ width: '50%', bgcolor: 'background.paper', display: 'flex', justifyContent:"space-between", alignItems:'center', flexDirection: 'column', margin: '0 auto' }}
    aria-label="contacts"
  >
    {users.map((user)=> {
        return(
    <ListItem  key={user.id}  sx={{border:'1px solid black'}}>
      <ListItemText primary={user.name} secondary={user.address}/>
      <ListItem sx={{ display: 'flex', justifyContent:"flex-end"}}>
        <div>
        <ListItemText primary={user.hobby} sx={{textAlign: 'center'}} />
      <div>
      <ListItemButton sx={{gap: '10px'}}>
        <NavLink to={{
          pathname: `/view/${user.id}`,
          state: {
            user: user
          }
        }} >
        <Button variant='contained'> View </Button>
        </NavLink>
        <Button onClick={() => {handleOpen(); handleUpdate(user.id)}} variant='contained'>Edit</Button>
      </ListItemButton>
      
      </div>
        </div>
      </ListItem>
    </ListItem>
        )
    })}
  </List>
  );
 }
};

export default UserList;
