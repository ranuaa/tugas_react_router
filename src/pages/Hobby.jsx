import React from "react";
import Appbar from "../components/Appbar";
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Stack from '@mui/material/Stack';
import { styled } from '@mui/material/styles';
import { useLocation } from "react-router-dom";

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));

const Hobby = () => {
  const location = useLocation()
  const data = location.state.users
  console.log(data)
  return (
  <div>
    <Appbar/>
    <Box sx={{ width: '100%' }}>
      <h1 style={{textAlign: 'center'}}>Hobby List</h1>
      <Stack spacing={2}>
        {data?.map((user) => {
        return(
          <Item>{user.hobby}</Item>
        )
        })}
      </Stack>
    </Box>
  </div>
  );
};

export default Hobby;
