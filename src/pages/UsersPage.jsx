import React from "react";
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { useLocation } from "react-router-dom";
import Appbar from "../components/Appbar";

const UsersPage = () => {
    const location = useLocation()
  return (
    <div>
    <Appbar/>
    <Card sx={{ maxWidth: 345, margin: '0', position: 'absolute', top:'35%', left: '40%' }}>
    <CardMedia
      sx={{ height: 140 }}
      image="https://picsum.photos/345/140"
      title="green iguana"
    />
    <CardContent>
      <Typography gutterBottom variant="h5" component="div">
        {location.state.user.name}
      </Typography>
      <Typography variant="body2" color="text.secondary">
      {location.state.user.hobby}
      </Typography>
    </CardContent>
    <CardActions>
      <Button onClick={() => alert(`He/She lives in " ${location.state.user.address} "`)} size="small">Click here to see the address</Button>
    </CardActions>
  </Card>
    </div>
  );
};

export default UsersPage;
