import * as React from "react";
import Backdrop from "@mui/material/Backdrop";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import Fade from "@mui/material/Fade";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import { TextField } from "@mui/material";

const style = {
 position: "absolute",
 top: "50%",
 left: "50%",
 transform: "translate(-50%, -50%)",
 width: 400,
 bgcolor: "background.paper",
 border: "2px solid #000",
 boxShadow: 24,
 p: 4,
};

const FormModal = ({
 open,
 setOpen,
 handleClose,
 handleOpen,
 handleChange,
 handleSubmit,
 formData
}) => {
 return (
  <div>
   <Modal
    aria-labelledby="transition-modal-title"
    aria-describedby="transition-modal-description"
    open={open}
    onClose={handleClose}
    closeAfterTransition
    slots={{ backdrop: Backdrop }}
    slotProps={{
     backdrop: {
      timeout: 500,
     },
    }}>
    <Fade in={open}>
     <Box sx={style}>
      <Typography
       id="transition-modal-title"
       variant="h6"
       component="h2"
       sx={{ textAlign: "center", marginBottom: "20px" }}>
       Add New User
      </Typography>
      <form onSubmit={handleSubmit}>
       <Typography>Name</Typography>
       <TextField 
       fullWidth 
       name='name' 
       value={formData.name}
       onChange={handleChange}
       />
       <Typography>Address</Typography>
       <TextField 
       fullWidth 
       value={formData.address}
       name='address' 
       onChange={handleChange}
       />
       <Typography>Hobby</Typography>
       <TextField 
       fullWidth  
       value={formData.hobby}
       name='hobby' 
       onChange={handleChange}
       />
       <Button
        variant="outlined"
        sx={{ marginTop: "20px", backgroundColor: "#00c2cb", color: "black" }}
        onClick={handleSubmit}
        >
        Save
       </Button>
      </form>
     </Box>
    </Fade>
   </Modal>
  </div>
 );
};

export default FormModal;
