import React from "react";
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import { Link, NavLink } from "react-router-dom";

const Appbar = ({handleOpen, users}) => {
  return (
    <Box sx={{ flexGrow: 1 }}>
    <AppBar position="static">
      <Toolbar>
      
        <div style={{
          display: 'flex',
          width: '50%'
        }}>
          <Link className="nav-link" to='/' style={{textDecoration:'none', color: 'white'}}>
        <Typography
            variant="h5"
            noWrap
            component="a"
            href="/"
            sx={{
              mr: 2,
              display: { xs: 'none', md: 'flex' },
              fontFamily: 'monospace',
              fontWeight: 700,
              letterSpacing: '.3rem',
              color: 'inherit',
              textDecoration: 'none',
              padding: '15px'
            }}
          >
            MyApp
          </Typography>
          </Link>
        <NavLink 
         className="nav-link"
        to={
          {
            pathname: '/about'
          }
        }
        activeStyle={{ borderBottom: '2px solid white'}}
        style={{textDecoration: 'none', color: 'white'}}
        >
        <Typography variant="h6" component="div" sx={{padding: '15px'}}>
          About
        </Typography>
        </NavLink>
        <NavLink 
        className="nav-link"
        to={
          {
            pathname: '/hobby',
            state: {
              users : users
            }
          }
        }
        activeStyle={{ borderBottom: '2px solid white'}}
        style={{textDecoration: 'none', color: 'white'}}
        >
        <Typography variant="h6" component="div" sx={{padding: '15px'}}>
          Hobby
        </Typography>
        </NavLink>
        </div>
        <div style={{width: '50%', display: 'flex', justifyContent: 'flex-end'}}>
        <Button color="inherit" variant='contained' sx={{backgroundColor:'#00c2cb'}} onClick={handleOpen} >Add User</Button>
        </div>
      </Toolbar>
    </AppBar>
  </Box>
  );
};

export default Appbar;
