import React from "react";
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Homepage from "./pages/Homepage";
import About from "./pages/About";
import Hobby from "./pages/Hobby";
import Appbar from "./components/Appbar";
import UsersPage from "./pages/UsersPage";


const Routers = () => {
  return (
    <>
    <Switch>
        <Route exact path='/' >
            <Homepage/>
        </Route>
    </Switch>

    <Switch>
        <Route exact path='/about' >
            <About/>
        </Route>
    </Switch>

    <Switch>
        <Route exact path='/hobby' >
            <Hobby/>
        </Route>
    </Switch>
    <Switch>
        <Route exact path='/view/:id' >
            <UsersPage/>
        </Route>
    </Switch>
    </>
    
  );
};

export default Routers;
